'use strict';

serviceJoinApp.controller('formCtrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
    $scope.stacksavingsForm = {};
        $scope.saveForm = function(file, form_name){
            var rString = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            var profileId = "profile" + rString
            imageUplaod.getSignedUrl(file.type, profileId).then(function(result){
              var src = result.data.resultUrl + '?v=' + Math.random();
              $scope.form.bgImage = src;
              $scope.form.formname = form_name;
              $scope.stacksavingsForm.data = {}
              var stacksavingsFormData = $scope.stacksavingsForm.data
              stacksavingsFormData.firstname = $scope.form.firstname
              stacksavingsFormData.lastname = $scope.form.lastname
              stacksavingsFormData.phone = $scope.form.phone
              stacksavingsFormData.email = $scope.form.email
              stacksavingsFormData.message = $scope.form.message
              stacksavingsFormData.bgImage = $scope.form.bgImage
              stacksavingsFormData.formname = $scope.form.formname

              angular.copy($scope.form);
              formService.saveForm($scope.stacksavingsForm).then(function(result){
                  debugger;
              })
                Upload.http({
                    method : "PUT",
                    url    : result.data.oneTimeUploadUrl,
                    headers: {
                            'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                        },
                    data   : file
                    }).then(function (resp) {
                        //$('.post-content-text').css('background-image', 'url(\'' + src + '\')');
                        //$('#thumbnail-img').hide();
                    },function (response) {
                        if (response.status > 0)
                        self.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        //$scope.picFile.progress = file.progress;
                });
            });
        }
    }]);

    serviceJoinApp.controller('form1Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
        $scope.stacksavingsForm = {};
        $scope.saveForm1 = function(){
          $scope.form1.formname = "webform";
          $scope.stacksavingsForm.data = angular.copy($scope.form1);
            formService.saveForm($scope.stacksavingsForm).then(function(result){
                debugger;
            })
        }
    }]);

    serviceJoinApp.controller('form2Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
      $scope.stacksavingsForm = {};
      $scope.saveForm2 = function(){
        $scope.form2.formname = "webform";
        $scope.stacksavingsForm.data = angular.copy($scope.form2);
          formService.saveForm($scope.stacksavingsForm).then(function(result){
              debugger;
          })
      }
    }]);


    serviceJoinApp.controller('form3Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
      $scope.stacksavingsForm = {};
      $scope.saveForm3 = function(){
        $scope.form3.formname = "webform";
        $scope.stacksavingsForm.data = angular.copy($scope.form3);
          formService.saveForm($scope.stacksavingsForm).then(function(result){
              debugger;
          })
      }
    }]);

    serviceJoinApp.controller("FeedCtrl", ['$scope','FeedService', '$location', '$sanitize', '$http', 'linkPost', function ($scope,Feed, $location, $sanitize, $http, linkPost) {
        $scope.loadButonText="Load";
        $scope.loadFeed=function(siteName, e){
          var feedSrc = ""

          var location = $location.search().location
          var query = $location.search().query

          if (siteName === "indeed") {

            feedSrc = "http://rss.indeed.com/rss?q=" + query + "&l=marlborough%2C+ma&start=100&_ga=1.122956683.1461890666.1468017775"

          } else {

            //feedSrc = "http://www.rssmix.com/u/8198632/rss.xml"

            feedSrc = "https://www.bing.com/news/search?q=" + query + "&go=Search&&format=rss"

          }

            Feed.parseFeed(feedSrc).then(function(res){

                var mustContain = []

                var items = res.data.responseData.feed.entries;

                var feeds = []

                var tweet = ""

                //do search
                angular.forEach(items, function(value, key) {
                  var content = value.content.toLowerCase()

                  var matched = true

                  angular.forEach(mustContain, function(mustContainValue, mustContainKey) {
                    mustContainValue = mustContainValue.toLowerCase()
                    var tmpMatch = content.indexOf(mustContainValue) >= 0
                      if (!tmpMatch) {
                        matched = false
                      }
                  });

                  if (matched) {
                    value.content = $sanitize(value.content)
                    value.contentSnippet = $sanitize(value.contentSnippet)
                    value.title = $sanitize(value.title)
                    if (tweet == "") {
                      tweet = value.title
                    }
                    feeds.push(value)
                  }

                });

                $scope.feeds = feeds

                if (location != undefined && location != "") {
                  var wikiSrc = "https://en.wikipedia.org/w/api.php?action=opensearch&search=" + location + "&limit=1&namespace=0&format=json&callback=JSON_CALLBACK"

                  $http.jsonp(wikiSrc).success(function (data) {

                    $scope.cityInfo = data[2][0]; // response data

                    var url = query.replace(/%20/g, "")
                    url = "/" + url.replace(/ /g, "") + ".html"

                     var dataObj = {
                       tweet : tweet,
                       url : url,
                       link : "<a href=" + url +">" + query + "</a> <br/>"
                     }
                     var linkObj = {data : dataObj,
                          auth_token : "2392fj3494jx",
                          tableName : "Links"
                      }
                     linkPost.addLink(linkObj).then(function(linkObj){
                         debugger;
                     })

                 }).error(function (data) {
                     console.log("failed");
                 });

                }

            });
        }
    }]);

    serviceJoinApp.factory('FeedService',['$http',function($http){
        return {
            parseFeed : function(url){
                var results = $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));

                return results

            }
        }
    }]);



function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
