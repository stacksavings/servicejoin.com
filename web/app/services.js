(function() {
	var app = angular.module('serviceJoinApp.services', []);

	app.factory('Template', ['$http', '$templateRequest', '$q', '$window', function($http, $templateRequest, $q, $window) {
		var path = "../../partials/";

		var template = new function() {

			var getTemplate = function(tpl) {

				return $templateRequest(tpl);
			};

			this.compile = function(tpl, data) {

				var defer = $q.defer();

				getTemplate(path + tpl).then(function(content) {
					var dataToRet = $window.Handlebars.compile(content)(data);
					defer.resolve(dataToRet);
				}, function(e) {
					defer.resolve('');
				});

				return defer.promise;
			};

		};

		return template;

	}]);

	app.factory('PageTextService', [
		"$rootScope",
		"$http",
		"$location",
		function($rootScope, $http,$location) {
			var page = $location.search().page;
			var auth_token = $location.search().auth_token;

			var service = {
				getPageItems: function () {
					var data = {template : $rootScope.template};
						if(auth_token!==undefined)
							data.auth_token = auth_token;
						if(page!==undefined)
							data.page = page;

                    return $http.post($rootScope.apiUrl, data);
                },
				getPage: function(){
					return page;
				},
				updatePageItem: function(items,key, tpl) {
					var fdata = {
                        // key       : key,
                        // value     : items[key],
                        data	  : items,
                       	page      : (page === undefined ? "default" : page),
                        auth_token: auth_token,
                        tplItem   : false,
                        tpl       : $rootScope.template,

                    };
					return $http.post($rootScope.updateItemsUrl, fdata, {headers: {'Content-Type': 'text/plain'}});
				},
				updatePageFullItem: function(items) {

					var curPage = page
					if (curPage != undefined) {
						//check if the page was set to a differnet value than current page, in case of new page creation
						if ($rootScope.mc.items.page != page) {
							curPage = $rootScope.mc.items.page
						}
					}

					var finalData = {

							page      : (curPage === undefined ? "default" : curPage),
	                        auth_token: auth_token,
	                        tplItem   : false,
	                        tpl       : $rootScope.template,
	                        data	  : items
					}
					return $http.post($rootScope.updateItemsUrl,finalData,{headers: {'Content-Type': 'text/plain'}});
				},
				createPage: function(new_page){
					// $rootScope.mc.items.page = new_page
					// PageTextService.updatePageFullItem($rootScope.mc.items).then(function (result) {
					// 	 console.log($rootScope.mc.items);
					//
					// 	 window.location.href = '../../templates/' + $rootScope.template + "/index.html?page="+new_page+"&auth_token="+auth_token;
					// });
				},
				getSignedUrl: function (file_type, img_name) {
                    var data = {
                        tpl        : $rootScope.template,
                        contentType: file_type,
                        imgName    : img_name,
                        page       : (page === undefined ? "Test" : page),
                        auth_token : auth_token
                    };
                    return $http.post($rootScope.signedUrl, data);
                }
			};

			return service;
		}
	]);

	app.factory('AddRemoveItemService', [
		"$rootScope",
		"$http",
		"$location",
		function($rootScope, $http,$location) {

			var page = $location.search().page;
			var auth_token = $location.search().auth_token;

			var service = {
				addMenuItem: function(title,href){
					var data = {
                        title       : title,
                        link       : href,
                        page      : (page === undefined ? "Test" : page),
                        auth_token: auth_token,
                        template: $rootScope.template,
                        itemtype:	"menu"
                    };

					return $http.post($rootScope.apiItemUrl, data);
				},
				removeMenuItem: function(title){
					var data = {
						title		: title,
						page		: (page === undefined ? "Test" : page),
						auth_token	: auth_token,
						itemtype	: "menu",
						template: $rootScope.template,
						toDelete	: true
					}

					return $http.post($rootScope.apiItemUrl,data);
				}
			};

			return service;
		}
	]);


	app.factory('FormInsertService', [
		"$rootScope",
		"$http",
		"$location",
		function($rootScope, $http,$location) {


			//var auth_token = $location.search().auth_token;

			var service = {
				addMenuItem: function(title,href){
					var data = {
				        email       : "test39@gmail.com"
				    	};

					return $http.post($rootScope.apiItemUrl, data);
				}
			};

			return service;
		}
	]);

	app.factory("EditFieldService",[function(){

	}])
})();
