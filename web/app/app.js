var serviceJoinApp = angular.module('serviceJoinApp', [
	'serviceJoinApp.controllers',
	'serviceJoinApp.services',
	'textAngular',
  //Template Specific
  'ngResource',
  'ngFileUpload'
  //END Template Specific
]);

//Template Specific
serviceJoinApp.constant('apiBaseUrl', 'https://z8mw90e1z1.execute-api.us-west-2.amazonaws.com/dev/');
//END Template Specific

(function() {

	angular.element(document).ready(function() {
		angular.bootstrap(document, ["serviceJoinApp"]);

		// remove prebuild xeditable class to have custom text classes untouched
		angular.element(".editable-click").removeClass('editable-click');
	});

	var baseUrl = "https://z8mw90e1z1.execute-api.us-west-2.amazonaws.com";

	angular.module('serviceJoinApp')
		.run(["$rootScope", function($rootScope) {
			// if will be more resources use without /page
			$rootScope.apiUrl = baseUrl + '/dev/read';
			$rootScope.updateItemsUrl = baseUrl + '/dev/updateFull';
			$rootScope.apiItemUrl = baseUrl + '/dev/createUpdateItems';
			$rootScope.signedUrl = baseUrl + '/dev/getSignedUrl';
			$rootScope.defaultTemplte = 'template1';
			$rootScope.bucketBaseURL = '';
		}]);

	angular.module('textAngular')
		.config(function($provide) {

			$provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
		        // $delegate is the taOptions we are decorating
		        // register the tool with textAngular
		        taRegisterTool('fontColor', {
		            display: "<button colorpicker type='button' class='btn btn-default ng-scope' title='Font Color' colorpicker-close-on-select  colorpicker-position='bottom' ng-model='fontColor' style='color: {{fontColor}}'><i class='fa fa-font '></i></button>",
		            action: function(deferred) {
		                var self = this;
						if (typeof self.listener == 'undefined') {
							self.listener = self.$watch('fontColor', function (newValue) {
								self.$editor().wrapSelection('foreColor', newValue);
							});
						}

						self.$on('colorpicker-selected', function () {
							deferred.resolve();
						});

						self.$on('colorpicker-closed', function () {
							deferred.resolve();
						});

						return false;
		            }
		        });

		        // add the button to the default toolbar definition
		        taOptions.toolbar[1].unshift('fontColor');
		        return taOptions;
		    }]);
		});
})();

function taClick(){

}
