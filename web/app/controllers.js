(function () {
    var app = angular.module('serviceJoinApp.controllers', ["xeditable", 'ngFileUpload', 'colorpicker.module']);
    var photos = [];

    app.config(['$locationProvider', '$httpProvider', function AppConfig($locationProvider, $httpProvider) {
        $locationProvider.html5Mode(true);
    }]);

    function mainController($rootScope, PageTextService, AddRemoveItemService, Upload, $timeout, $http, $location, $window) {
        var self   = this;
        $rootScope.mc=this;
        self.items = {};
        self.tplItems = {};

        self.showStat = [];

        var path = $location.absUrl();
        var match = path.match(/.*\/templates\/(.*)\/.*/);
        console.log(match[1]);
        $rootScope.template = match[1] || 'template1';
        //$rootScope.template = 'blog-template';
        //$rootScope.authenticated = false;
        $rootScope.page          = "default";
        $rootScope.dataLoaded = false;
        $rootScope.setColor = function(modelname){
            return { color: modelname };
        }
        self.new_page            = "";
        self.changeGroup         = 'one';

        PageTextService.getPageItems().then(function (result) {
                $rootScope.dataLoaded = true;
                if (result.data.errorMessage != undefined) {
                    return console.log(result);
                }
                angular.forEach(result.data.resValues, function(item, key){
                    result.data.resValues[key] = item;
                });
                $rootScope.bucketBaseURL = result.data.bucketUrl;
                $rootScope.authenticated = result.data.auth_status;

                //for demo mode
                 var auth_token = $location.search().auth_token;
                 if (auth_token === "demo") {
                   //saving edits won't actually work since the auth_token isn't actually valid but this allows
                   //the user to see the admin panels and play around with edit mode
                   $rootScope.authenticated = true
                   $rootScope.demomode = true
                 }

                $rootScope.page = PageTextService.getPage();
                self.items = result.data.resValues;
                self.tplItems = result.data.tplValues;
               /* if(self.items['bgImage']){
                    $timeout(function(){
                        $('#benefit').parent().css('background-image', 'url(\'' + self.items['bgImage'] + '\')');
                    }, 300);
                }*/

                //hide the loading screen
                if (window.loading_screen) {
                  window.loading_screen.finish()
                }

            });
        self.goto = function(link){
            if(!$rootScope.authenticated){
                window.location.href = link;
            }
        }
        self.updateData = function (key, tpl) {
            self.showStat[key] = !self.showStat[key];

            PageTextService.updatePageItem(
                tpl ? self.tplItems : self.items,
                key,
                tpl
            ).then(function (result) {
                if (result.data.errorMessage != undefined) {
                    return alert(result.data.errorMessage);
                }
            });
        };
        self.updateItem = function (value, key, tpl) {
            /*if(tpl) self.tplItems[key] = value;
            else self.items[key] = value;*/
            /*self.updateDialog(value, key, tpl);*/
        };
        self.updateGroupItem = function (value, key, group, tpl) {
             if (self.changeGroup == 'one') {
                 self.updateDialog(value, key, tpl);
             }
             else {
                 $('.'+group).each(function (){
                     self.updateDialog(value, $(this).attr('color'), tpl);
                 });
             }

        };

        self.updateDialog = function(value, key, tpl) {
            if(tpl) self.tplItems[key] = value;
            // else self.items[key] = value;
            PageTextService.updatePageFullItem(self.items).then(function (result) {

            });
        };

        self.createPage = function () {
            if (self.new_page !== "" && $rootScope.authenticated) {
              $rootScope.mc.items.page = self.new_page
    					PageTextService.updatePageFullItem($rootScope.mc.items).then(function (result) {
    						 console.log($rootScope.mc.items);
                 var auth_token = $location.search().auth_token;
    						 window.location.href = '../../templates/' + $rootScope.template + "/index.html?page="+self.new_page +"&auth_token="+auth_token;
    					});
            }
        };

        self.editField = function (editForm) {
            console.log(editForm);
            if ($rootScope.authenticated) editForm.$show();
        };
        self.saveTitle = function () {
            if ($rootScope.authenticated) {
                PageTextService.updatePageItem(self.items, 'title');
            }
        };

        self.uploadFiles = function (modelName, bindDivId, bindOption, divId, element) {
            var file = self.files;
            var fileName = file.name;
            self.f = file;
            //self.errFile = errFiles && errFiles[0];
            if (file) {
                PageTextService
                    .getSignedUrl(file.type, fileName)
                    .then(function (result) {
                        file.upload = Upload.http({
                            method : "PUT",
                            url    : result.data.oneTimeUploadUrl,
                            headers: {
                                'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                            },
                            data   : file
                        });

                        var src = result.data.resultUrl + '?v=' + Math.random();

                        var $progress = element.find('.progress');

                        file.upload.then(function (response) {
                            if(bindOption == 'forground'){
                                 $('#' + bindDivId).attr('src', src);
                            }else{
                                 $('#' + bindDivId).css('background-image', 'url(\'' + src + '\')');
                            }

                            var image = new Image();
                            image.onload = function() {
                                //$progress.html(100);
                            };
                            image.src = src;
                            self.items[modelName] = src;
                            self.updateDialog(src, modelName, false);
                            $timeout(function () {
                                file.result = response.data;
                            });
                        }, function (response) {
                            if (response.status > 0)
                                self.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 *evt.loaded / evt.total));
                           /* $rootScope.mc.files.progress = file.progress;*/
                            $progress.html(file.progress);
                        });
                    });
            }
        }

        self.addItem = function(itemtype){
            if(itemtype=="menu"){
                var title = self.menu_item_title;
                var href = self.menu_item_href;
                if((!title || title==='') && !href){
                    alert("Invalid values")
                    return;
                }
                self.items.items.menu[title] = {'link':href};
                PageTextService.updatePageFullItem(self.items).then(function (result) {
                    if(result){
                        self.items.items.menu[title] = {};
                        self.items.items.menu[title].link = href;
                    }else{
                        alert(result.data.errorMessage);
                    }
                });
            }
        }

        self.deleteMenuItem = function(obj,key){
            var element = obj.target;
            delete self.items.items.menu[key];
            PageTextService.updatePageFullItem(self.items).then(function (result) {
                if(element.parentElement.nodeName=='A'){
                    element.parentElement.parentElement.remove();
                }else if(element.parentElement.nodeName=='LI'){
                    element.parentElement.remove();
                }
            });
        }

    };

    app.controller("MainController", [
        "$rootScope",
        "PageTextService",
        "AddRemoveItemService",
        "Upload",
        "$timeout",
        "$http",
        "$location",
        mainController
    ]);


	app.controller('FormCtrl', function ($scope, $http) {
        console.log('Inside FormCtrl');
	    var formData = {
		firstname: "default",
		emailaddress: "default",
		textareacontent: "default",
		gender: "default",
		member: false,
		file_profile: "default",
		file_avatar: "default"
	    };

	    $scope.save = function() {
		formData = $scope.form;
	    };

	    $scope.submitForm = function() {
alert('submit')
		console.log("posting data....");
		formData = $scope.form;
		console.log(formData);
		//$http.post('form.php', JSON.stringify(data)).success(function(){/*success callback*/});
	    };

	 });


    app.directive("editableField", ['$rootScope', '$compile', 'Template', '$timeout', 'PageTextService', 'Upload', function ($rootScope, $compile, Template, $timeout, PageTextService, Upload) {
        return {
            scope: true,
            link: function (scope, element, attr) {
                    var tplItem    = attr.tplItem !== undefined;
                    var bindPrefix =  "mc.items.";
                    if(!$rootScope.attrArray)
                        $rootScope.attrArray = new Object();

                    if(!$rootScope.elArray) {
                        $rootScope.elArray = new Object();
                    }

                    attr.modelname = attr.ngBind.substring(9);
                    attr.color = attr.ngStyle.substring(16, attr.ngStyle.length-1);
                    attr.tagname = element[0].localName;
                    attr.hrefname = attr.ngBind + 'Href';
                    var hide_id = attr.modelname;
                    var bind = attr.ngBind;
                    scope.closeWysiwyg = function(e){ console.log(e); };

                    scope.updateItems = function(value, key){
                        PageTextService.updatePageFullItem(scope.mc.items).then(function (result) {
                            attr = $rootScope.attrArray[bind];
                            element = $rootScope.elArray[bind];
                            if(attr){
                                bind = attr.ngBind;
                                console.log(bind);
                            }
                            if(attr.wysiwyg) {
                                Template.compile("wysiwyg.html",{attr:attr, bindPrefix: bindPrefix, bind:bind, tplItem:tplItem}).then(function(html){
                                    var e    = $compile(html)(scope);
                                    element.html(e);
                                    $(element).click(function(){
                                        $(this).children('form').draggable({
                                          cursor: "crosshair"
                                        });
                                        if(!$("#"+hide_id).length){
                                            var text = '<span id="'+hide_id+'" class="hide-wysiwyg-form"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                             $(this).children('form').prepend(text);

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children().addClass('btn btn-sm btn-info');

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children("button[type='button']").addClass('btn btn-sm btn-danger');
                                            $('#'+hide_id).click(function(e){
                                                scope.$$childTail.$form.$hide();
                                            });
                                        }
                                    });
                                });
                            }else {
                                Template.compile('editableField-tpl.html', {attr: attr, bind: bind, bindPrefix: bindPrefix, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }
                        });
                    }

                    $rootScope.$watch("authenticated", function (newValue, oldValue) {
                        if(tplItem) {
                            if(!scope.mc.tplItems[attr.modelname]) scope.mc.tplItems[attr.modelname] = attr.modelname;
                        } else {
                            if(!scope.mc.items[attr.modelname]) scope.mc.items[attr.modelname] = '<new-value>';
                        }
                        if($rootScope.authenticated){
                            $rootScope.attrArray[bind] = attr;
                            $rootScope.elArray[bind] = element;

                            if (attr.wysiwyg) {
                                Template.compile("wysiwyg.html",{attr:attr, bindPrefix: bindPrefix, bind:bind, tplItem:tplItem}).then(function(html){
                                    var e = $compile(html)(scope);
                                    element.html(e);
                                    $(element).click(function(){
                                        $(this).children('form').draggable({
                                          cursor: "crosshair"
                                        });
                                        $("form.editable-textareaw").addClass('formEditable');
                                        if(!$("#"+hide_id).length){
                                            var text = '<span id="'+hide_id+'" class="hide-wysiwyg-form"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                            /*var text = '<button type="button" id="'+hide_id+'" class="hide-wysiwyg-form" ><i class="fa fa-times" aria-hidden="true"></i></button>';*/
                                            /*$(this).children('form').prepend($compile(text)(scope));*/
                                            $(this).children('form').prepend(text);

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children().addClass('btn btn-sm btn-info');

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children("button[type='button']").addClass('btn btn-sm btn-danger');
                                            $('#'+hide_id).click(function(e){
                                                scope.$$childTail.$form.$hide();
                                            });
                                        }

                                    });
                                });

                            }
                            /*else if (attr.editimage) {
                                Template.compile('ediableFieldImage.html', {attr: attr, bindPrefix: bindPrefix, bind: bind, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }*/
                            else if (attr.background) {
                                Template.compile('editableFieldBackground-tpl.html', {attr: attr, bindPrefix: bindPrefix, bind: bind, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }
                            else {
                                $rootScope.attrArray[bind] = attr;
                                $rootScope.elArray[bind] = element;
                                Template.compile('editableField-tpl.html', {attr: attr, bind: bind, bindPrefix: bindPrefix, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }
                        }
                        else{
                            if (attr.wysiwyg) {
                                Template.compile("wysiwygTemplate.html",{attr:attr, bindPrefix: bindPrefix, bind:bind, tplItem:tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                            else if (attr.typename) {
                                Template.compile("typenameTemplate.html",{attr:attr,bind:bind, bindPrefix:bindPrefix}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }

                            /*else if (attr.editimage) {
                                Template.compile("editableImage.html",{attr:attr,bind:bind, bindPrefix:bindPrefix}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }*/

                            else if (attr.background) {
                                Template.compile("backgroundTemplate.html",{attr:attr,bind:bind, bindPrefix:bindPrefix}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                            else if (attr.tagname == 'a') {
                                Template.compile("anchorTemplate.html",{attr:attr, bindPrefix:bindPrefix, bind:bind}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                            else{
                                Template.compile("defaultTemplate.html",{attr:attr, bindPrefix:bindPrefix, bind:bind}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }

                        }
                    });
            },

        }
    }]);
    app.directive("editableFieldImage", ['$rootScope', '$compile', 'Template', '$timeout', 'PageTextService', 'Upload', function ($rootScope, $compile, Template, $timeout, PageTextService, Upload) {
        return {
            link: function (scope, element, attr) {
                    scope.uploadImage = function (modelName, bindDivId, bindOption) {
                        console.log(modelName);
                        var file = scope.files;
                        var fileName = file.name;
                        if (file) {
                            PageTextService.getSignedUrl(file.type, fileName).then(function (result) {
                                    file.upload = Upload.http({
                                        method : "PUT",
                                        url    : result.data.oneTimeUploadUrl,
                                        headers: {
                                            'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                                        },
                                        data   : file
                                    });
                                    var src = result.data.resultUrl + '?v=' + Math.random();
                                    /*var append = element.after('<span class="progress"></span>');
                                    var $progress = append.find('.progress');*/
                                    file.upload.then(function (response) {
                                        if(bindOption == 'forground'){
                                             $('#' + bindDivId).attr('src', src);
                                        }else{
                                             $('#' + bindDivId).css('background-image', 'url(\'' + src + '\')');
                                        }
                                        scope.mc.items[modelName] = src;
                                        scope.mc.updateDialog(src, modelName, false);
                                        $timeout(function () {
                                            file.result = response.data;
                                        });
                                    }, function (response) {
                                        if (response.status > 0)
                                            scope.mc.errorMsg = response.status + ': ' + response.data;
                                    }, function (evt) {
                                        file.progress = Math.min(100, parseInt(100.0 *evt.loaded / evt.total));
                                        scope.mc.items[modelName].progress = file.progress
                                        /*$progress.html(file.progress);*/
                                    });
                                });
                        }
                    }
                    $rootScope.$watch("authenticated", function (newValue, oldValue) {
                        var src = scope.mc.items[attr.modelname];
                        if(src){
                            if(attr.bindOption == 'forground'){
                                $('#'+attr.bindDivid).attr('src', src);
                            }else{
                                $('#'+attr.bindDivid).css('background-image', 'url(' + src + ')');
                            }
                        }
                       if($rootScope.authenticated){
                            Template.compile('ediableFieldImage.html', {attr: attr}).then(function(html) {
                                    element.html($compile(html)(scope));
                            });
                        }
                    });
            },

        }
    }]);
    app.directive("bgImage", function ($rootScope, $compile) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$watch("page", function (newValue, oldValue) {

		    var bgImageName = attrs.bgImage;

		    if (bgImageName.split('\/').length > -1) {
			//if it is a path, indstead of just an image name, we take just the image name as we are going to construct a new path
			bgImageName = bgImageName.substring(bgImageName.lastIndexOf("\/") + 1, bgImageName.length);
		    }

		    //($rootScope.page === undefined ? 'default' : $rootScope.page)
                    var src = $rootScope.bucketBaseURL + 'templates/' + $rootScope.template + '/img/' + 'default' + '/' + bgImageName;
                    var image = new Image();
                    image.onload = function() {
                        element.css('background-image', 'url(' + src + ')');
                    };
                    image.src = src;
                })
            }
        }
    });

    app.directive("editableImage", function ($rootScope, $compile,Template, $timeout, PageTextService, Upload) {
        return {
            restrict: 'A',
            scope: true,
            link: function (scope, element, attrs) {
                scope.uploadFiles = function (modelName, bindDivId, bindOption, divId) {
                    var file = scope.files;
                    var fileName = file.name;
                    if (file) {
                        PageTextService.getSignedUrl(file.type, fileName).then(function (result) {
                                file.upload = Upload.http({
                                    method : "PUT",
                                    url    : result.data.oneTimeUploadUrl,
                                    headers: {
                                        'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                                    },
                                    data   : file
                                });
                                var src = result.data.resultUrl + '?v=' + Math.random();
                                var $progress = element.find('.progress');
                                file.upload.then(function (response) {
                                    if(bindOption == 'forground'){
                                         $('#' + bindDivId).attr('src', src);
                                    }else{
                                         $('#' + bindDivId).css('background-image', 'url(\'' + src + '\')');
                                    }
                                    scope.mc.items[modelName] = src;
                                    scope.mc.updateDialog(src, modelName, false);
                                    $timeout(function () {
                                        file.result = response.data;
                                    });
                                }, function (response) {
                                    if (response.status > 0)
                                        scope.mc.errorMsg = response.status + ': ' + response.data;
                                }, function (evt) {
                                    file.progress = Math.min(100, parseInt(100.0 *evt.loaded / evt.total));
                                   /* $rootScope.mc.files.progress = file.progress;*/
                                    $progress.html(file.progress);
                                });
                            });
                    }
                }
                $rootScope.$watch("authenticated", function (newValue, oldValue) {
                    attrs.element = element;
                    if ($rootScope.authenticated) {
                        Template.compile("editableImage.html",{attrs:attrs}).then(function(html){
                            var e  = $compile(html)(scope);
                            element.prepend(e);
                        });
                        var src = scope.mc.items[attrs.modelname];
                        if(src){
                            if(attrs.bindOption == 'forground'){
                                    $('#'+attrs.bindDivid).attr('src', src);
                            }else{
                                    $('#'+attrs.bindDivid).css('background-image', 'url(' + src + ')');
                            }
                        }
                    }else{
                        var src2 = scope.mc.items[attrs.modelname];
                        if(src2){
                            if(attrs.bindOption == 'forground'){
                                    $('#'+attrs.bindDivid).attr('src', src2);
                            }else{
                                    $('#'+attrs.bindDivid).css('background-image', 'url(' + src2 + ')');
                            }
                        }
                    }
                })
            }
        }
    });

    app.directive("editableParallax", function ($rootScope, $compile,Template, $timeout, PageTextService, Upload) {
        return {
            restrict: 'A',
            scope: true,
            link: function (scope, element, attr) {
                scope.uploadParallax = function(modelName, bindDivId){
                    var file = scope.parallax;
                    var fileName = file.name;
                    if (file) {
                        PageTextService
                            .getSignedUrl(file.type, fileName)
                            .then(function (result) {
                                file.upload = Upload.http({
                                    method : "PUT",
                                    url    : result.data.oneTimeUploadUrl,
                                    headers: {
                                        'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                                    },
                                    data   : file
                                });
                                var src = result.data.resultUrl + '?v=' + Math.random();
                                file.upload.then(function (response) {
                                    $('#'+bindDivId).css('background-image', 'url(' + src + ')');
                                    scope.mc.items[modelName] = src;
                                    scope.mc.updateDialog(src, modelName, false);

                                }, function (response) {
                                    if (response.status > 0)
                                        scope.errorMsg = response.status + ': ' + response.data;
                                }, function (evt) {
                                    file.progress = Math.min(100, parseInt(100.0 *evt.loaded / evt.total));
                                    scope.parallax.progress = file.progress;
                                });
                            });
                    }
                }
                $rootScope.$watch("authenticated", function (newValue, oldValue) {
                    var src = scope.mc.items[attr.modelname];
                    if(src){
                        $('#'+attr.bindDivid).css('background-image', 'url(' + src + ')');
                    }
                    if ($rootScope.authenticated) {
                        Template.compile("editableParallax.html",{attr:attr}).then(function(html){
                            var e  = $compile(html)(scope);
                            element.prepend(e);
                        });
                    }
                })
            }
        }
    });

    app.directive("editableBg", function ($rootScope, $compile,Template, $timeout) {
        return {
            restrict: 'A',
            scope: true,
            link: function (scope, element, attrs) {
                var tplItem = attrs.tplItem !== undefined;
                if(tplItem) {
                    if(!scope.mc.tplItems[attrs.modelname]) scope.mc.tplItems[attrs.modelname] = attrs.modelname;
                } else {
                    if(!scope.mc.items[attrs.modelname]) scope.mc.items[attrs.modelname] = attrs.modelname;
                }
                var bindPrefix =  "mc.items.";
                var bind = attrs.modelname;
                $rootScope.$watch("authenticated", function (newVal, oldVal) {
                    scope.changeBgColor = function(value, model){
                        scope.mc.items[model] = value;
                        element.css('background', value);
                        scope.mc.updateDialog(value, model, false);
                    }

                    if ($rootScope.authenticated) {
                        Template.compile("editableBg.html",{attrs:attrs,bindPrefix:bindPrefix, bind:bind}).then(function(html){
                            element.prepend($compile(html)(scope));
                        });
                    }
                    $rootScope.$watch("page", function () {
                        element.css('background', (tplItem ? scope.mc.tplItems[attrs.modelname] : scope.mc.items[attrs.modelname]) || attrs.defaultvalue);
                    });
                 });
            }
        }
    });

    app.directive("draggableElem",function($rootScope){
        return {
            link: function(scope,element,attrs){
                $(element).draggable({
                  cursor: "crosshair"
                });
            }
        }
    })

    app.directive("itemtype",function($rootScope,$compile,Template){
        return{
            link: function(scope,element,attrs){
                switch(attrs.itemtype){
                    case "menu":
                        $rootScope.$watch("items",function(newVal,oldVal){
                            Template.compile("menuItem.html").then(function(html){
                                var e = $compile(html)(scope);
                                element.html(e);
                                //scope.$apply();
                            })
                        })
                        break;
                    default:
                        break;
                }
            }
        }
    })

    app.directive("editableItem", ['$rootScope', '$compile', 'Template', '$timeout', 'PageTextService', 'Upload', function ($rootScope, $compile, Template, $timeout, PageTextService, Upload) {
        return {
            scope: true,
            link: function (scope, element, attr) {
                    var tplItem    = attr.tplItem !== undefined;
                    var bindPrefix =  "mc.items.";
                    attr.color = attr.ngStyle.substring(7, attr.ngStyle.length-1);
                    attr.tagname = element[0].localName;
                    attr.hrefname = attr.modelname + 'Href';
                    var bind = attr.modelname;
                    var hide_id = attr.modelname;

                    scope.closeWysiwyg = function(e){ console.log(e); };
                    scope.updateFullItems = function(){
                        PageTextService.updatePageFullItem(scope.mc.items).then(function (result) {
                           console.log(scope.mc.items);
                        });
                    }

                    $rootScope.$watch("authenticated", function (newValue, oldValue) {
                        if(tplItem) {
                            if(!scope.mc.tplItems[attr.modelname]) scope.mc.tplItems[attr.modelname] = attr.modelname;
                        } else {
                            if(!scope.mc.items[attr.modelname]) scope.mc.items[attr.modelname] = '<new-value>';
                        }
                        if($rootScope.authenticated){
                            if (attr.wysiwyg) {
                                Template.compile('wysiwyg.html', {attr: attr, bind: bind, bindPrefix: bindPrefix, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                    $(element).click(function(){
                                        $(this).children('form').draggable({
                                          cursor: "crosshair"
                                        });
                                        $("form.editable-textareaw").addClass('formEditable');
                                        if(!$("#"+hide_id).length){
                                            var text = '<span id="'+hide_id+'" class="hide-wysiwyg-form"><i class="fa fa-times" aria-hidden="true"></i></span>';
                                            $(this).children('form').prepend(text);

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children().addClass('btn btn-sm btn-info');

                                            $("form.editable-textareaw").children('.editable-controls').children('.editable-buttons-bottom').children("button[type='button']").addClass('btn btn-sm btn-danger');
                                            $('#'+hide_id).click(function(e){
                                                scope.$$childTail.$form.$hide();
                                            });
                                        }

                                    });
                                });
                            }
                            /*else if (attr.background) {
                                Template.compile('editableFieldBackground-tpl.html', {attr: attr, bindPrefix: bindPrefix, bind: bind, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }*/
                            else{
                                Template.compile('editableItem-tpl.html', {attr: attr, bind: bind, bindPrefix: bindPrefix, tpl: tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                });
                            }
                        }else{
                            if (attr.wysiwyg) {
                                Template.compile("wysiwygTemplate.html",{attr:attr, bindPrefix: bindPrefix, bind:bind, tplItem:tplItem}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                            else if (attr.typename) {
                                Template.compile("typenameTemplate.html",{attr:attr,bind:bind, bindPrefix:bindPrefix}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }

                            /*else if (attr.background) {
                                Template.compile("backgroundTemplate.html",{attr:attr,bind:bind, bindPrefix:bindPrefix}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }*/
                            else if (attr.tagname == 'a') {
                                Template.compile("anchorTemplate.html",{attr:attr, bindPrefix:bindPrefix, bind:bind}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                            else{
                                Template.compile("defaultTemplate.html",{attr:attr, bindPrefix:bindPrefix, bind:bind}).then(function(html) {
                                    element.html($compile(html)(scope));
                                })
                            }
                        }
                    });
            },

        }
    }]);

})();
