"use strict";

var Step = require('step');

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 20000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if (!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady): onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};



var fs = require('fs');
var links = new Array();
//page.settings.javasciptEnabled = true;
//page.settings.resourceTimeout = 10000;


/** Read links from text file **/

links = fs.read('pages/links.txt').split("\n");

//links.push('exit.phantom.now'); //end of array that tells phantom to exit

var numOfLinks = links.length;
console.log("   Number of links: " + numOfLinks);

var i = 0;
var inValidLinks = [];
var failedToLoad = [];

var count = 0

var pause = false

var delay = 0


var asyncLoop = function(o){
    var i=-1,
        length = o.length;

    var loop = function(){
        i++;
        if(i==length){o.callback(); return;}
        o.functionToLoop(loop, i);
    }
    loop();//init
}


//for (i; i < numOfLinks; i++) {

asyncLoop({
    length : numOfLinks,
    functionToLoop : function(loop, i){


    console.log("");
    console.log(i + ". Link: " + links[i]);

    var isValidUrl = new RegExp('/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/').test(links[i]);
    console.log("   Is valid URL: " + isValidUrl);

    //if (links[i]=='exit') { var endOfFile = "exit";}

    if (isValidUrl) {


        // Extracting template name
        var templateName = new RegExp('templates(.*\/)').exec(links[i]);

        // Extracing URL page parameter value
        var pageName = new RegExp('[\?&]page=([^&#]*)').exec(links[i]);


        if (pageName != null) {
            var dirName = pageName[1];
        }
        if (templateName != null) {
            var tplName = templateName[1];
        }

        pageName ? console.log("    index.html will be placed in: " + templateName[1] + pageName[1]) : console.log("   There is no page parameter! index.html will be placed in root of deploy" + templateName[1] + " folder");
        console.log("");

        /**Start rendering page**/
        var fullLink = links[i];
        var numOfIterations = [];

        var savePage = function(loop, url, tName, fName) {

            var page = require('webpage').create();


            page.open(url,
                /*  prerender server    /    your AngularJS SPA  */

                page.onLoadStarted = function() {

                    console.log('');
                    console.log('');
                    console.log('Begin Loading page ...' + url);


                },

                page.onResourceTimeout = function(request) {
                    console.log('Time out occurred (#' + request.id + '): ' + JSON.stringify(request));
                    loop()
                },
                /*page.onConsoleMessage = function (msg, lineNum, sourceId) {
                    console.log(msg);
                    },

                 page.onResourceReceived = function(response) {
                  console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
                    }, */

                page.onLoadFinished = function(status) {


                    console.log('');
                    console.log('');
                    console.log("Status: " + status + " for loading page: " + page.url);



                    if (status === "success") {

                        setTimeout(function() {
                            var query = undefined
                            var matches = page.url.match(/query=([^&]*)/);
                            if (matches != undefined && matches != null) {
                                query = matches[1]
                            }


                            var content = page.content;

                            console.log('');
                            console.log('');
                            console.log("Begin removals...");
                            console.log('');

                            console.log('\nRemove <!-- Remove sections');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');
                            content = content.replace(/<!-- Remove-->[\s\S]*?<!-- END Remove-->/, '');

                            content = content.replace("<!--AdminModeDisabled", "")
                            content = content.replace("<!--AdminModeDisabled", "")
                            content = content.replace("<!--AdminModeDisabled", "")
                            content = content.replace("<!--AdminModeDisabled", "")
                            content = content.replace("<!--AdminModeDisabled", "")
                            content = content.replace("<!--AdminModeDisabled", "")

                            content = content.replace("AdminModeDisabled-->", "")
                            content = content.replace("AdminModeDisabled-->", "")
                            content = content.replace("AdminModeDisabled-->", "")
                            content = content.replace("AdminModeDisabled-->", "")
                            content = content.replace("AdminModeDisabled-->", "")
                            content = content.replace("AdminModeDisabled-->", "")

                            //var replaceThis = /((<.*link rel=.*).\app.*>)+/gi;
                            //console.log('Removing links... ' );
                            //content = content.replace(replaceThis, '');

                            //replaceThis = /((<.*script src=.*).\app.*>)+/gi;
                            // console.log('Removing links... ');
                            // content = content.replace(replaceThis, '');
                            //
                            // replaceThis = /((<.*script src=.*).\admin-panel.*>)+/gi;
                            // console.log('Removing links... ');
                            // content = content.replace(replaceThis, '');
                            //
                            // replaceThis = /((<.*script src=.*).bootstrap-colorpicker-module.min.js.*>)+/gi;
                            // console.log('Removing links... ');
                            // content = content.replace(replaceThis, '');

                            console.log('\nReplacing <base href="..."> ...');
                            var base = '/';
                            content = content.replace(/(<base href=.*>)+/gi, '<base href="' + base + '">');

                            console.log('\nRemove textAngular input.');
                            content = content.replace(/<input id="textAngular-editableFix-010203040506070809" class="ta-hidden-input" aria-hidden="true" unselectable="on" tabindex="-1">/, '');

                            console.log('Replace ng- tags... ');
                            //framework-level ng- tags are prefixed with * so that they can be easily replaced
                            content = content.replace(new RegExp('( \*\=\"\" ng-)', 'g'), " angulardisabled");
                            content = content.replace(new RegExp('( \\* ng-)', 'g'), " angulardisabled");
                            //content = content.replace(new RegExp('( \* ng-)', 'g'), " angulardisabled");

                            content = content.replace(new RegExp('(ng-repeat)', 'g'), "angulardisabled");

                            //var absUrl = /(background-image)(:\surl\()(https?:\/\/.*\/)/gi;
                            //console.log('Making links relative... ');
                            //content = content.replace(absUrl, 'background-image: url(img/default/');


                            //content = content.replace(replaceThis, '<!-- <script src="app/lib/angular/angular.js"></script> -->');

                            //This script writes file in folder from where is called
                            //so if you call it from servicejoin.com it will write rendered HTML
                            //in servicejoin.com/deploy/<index.html>



                            if (query != null && query != undefined && query != "") {
                                //clean up query for filename format
                                query = query.replace(/%20/g, "");
                                console.log("--write file")
                                fName ? fs.write('deploy' + tName + fName + '/' + query + '.html', content, 'w') : fs.write('deploy' + tName + 'index.html', content, 'w');
                            } else {
                                console.log("--write file")
                                fName ? fs.write('deploy' + tName + fName + '/index.html', content, 'w') : fs.write('deploy' + tName + 'index.html', content, 'w');
                            }


                            numOfIterations.push(1);

                            console.log('');
                            console.log('');
                            console.log("End of rendering.");
                            console.log('');
                            console.log("PAGE: " + (numOfIterations.length / 2) + " of " + (numOfLinks - (inValidLinks.length + failedToLoad.length)) + " FINISHED");

                            if ((numOfIterations.length / 2) >= numOfLinks - (inValidLinks.length + failedToLoad.length)) {

                                phantom.exit(); //End rendering pageS
                            }
                            loop()
                        }, 2000);



                    } else {

                        console.log('');
                        console.log("Page " + links[i] + " " + status + "ed to load.")
                        failedToLoad.push(links[i]);
                        loop()
                    }

                    //page.close();

                });

        }

        savePage(loop, fullLink, tplName, dirName)

//            functionToLoop : savePage(loop, fullLink, tplName, dirName),
        // function(loop, i){
        //     setTimeout(function(){
        //         document.write('Iteration ' + i + ' <br>');
        //         loop();
        //     },1000);
        // }



    } else if (!isValidUrl) {


        inValidLinks.push(links[i]);

        console.log("   InValid URL:  " + links[i]);
        //phantom.exit();
    }

  },
  callback : function(){
      console.log('All done!');
  }
});


console.log("");
console.log("End of text file");
